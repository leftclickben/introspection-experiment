import { ModuleIntrospectionResults } from './types';

const introspectionCache: Record<string, ModuleIntrospectionResults> = {};

export const memo = (key: string, generator: () => ModuleIntrospectionResults) => {
  if (!introspectionCache.hasOwnProperty(key)) {
    introspectionCache[key] = generator();
  }
  return introspectionCache[key];
};
