import { isSdkParameterConstant, isSdkParameterInput, ModuleIntrospectionResults } from './types';

export const getReportValue = (results: ModuleIntrospectionResults) => ({
  interestingExports: results.interestingExports.map(({ sdkParameters, ...interestingFunction }) => ({
    ...interestingFunction,
    sdkParameters: Object.keys(sdkParameters).reduce(
      (result, key) => {
        const param = sdkParameters[key];
        if (isSdkParameterInput(param)) {
          return { ...result, [key]: Array.from(param.possibleValues.values()) };
        }
        if (isSdkParameterConstant(param)) {
          return { ...result, [key]: [param.value] };
        }
        return { ...result, [key]: ['*'] };
      },
      {})
  }))
});
