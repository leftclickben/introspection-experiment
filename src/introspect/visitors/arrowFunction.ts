import { dirname, join } from 'path';
import { NodePath, Visitor } from '@babel/core';
import { InterestingFunction, isSdkParameterInput, VariableDeclaratorTraverseState } from '../types';
import {
  CallExpression,
  ImportSpecifier,
  isArrowFunctionExpression,
  isCallExpression,
  isIdentifier,
  isImportDeclaration,
  isMemberExpression,
  isStringLiteral,
  isVariableDeclarator,
  MemberExpression,
  ObjectExpression,
  ObjectProperty
} from '@babel/types';
import { introspectModule } from '../module';

// TODO Make the target modules (i.e. `aws-sdk`) configurable
const isAwsSdkImport = /^aws-sdk(\/.+)?$/;
const isInternalImport = /^\.\.?\//;

// TODO Support normal (non-arrow) functions
export const arrowFunctionVisitor: Visitor<VariableDeclaratorTraverseState> = {
  Identifier: (path, { basePath, moduleName, exportName, interestingExports, interestingLocals }) => {
    console.info(`Got identifier: ${JSON.stringify(path.node)}`);
    const binding = path.scope.getBinding(path.node.name);
    if (binding) {
      console.info(`Got binding for "${path.node.name}": ${JSON.stringify(binding.path.node)}`);
      console.info(`parent = ${JSON.stringify(binding.path.parentPath.node)}`);

      const bindingParentNode = binding.path.parentPath.node;
      if (isImportDeclaration(bindingParentNode)) {
        const importedModuleName = bindingParentNode.source.value;

        const normalisedImportedModuleName = isInternalImport.test(importedModuleName)
          ? join(dirname(moduleName), importedModuleName)
          : importedModuleName;

        console.info(`*** Found an import specifier, source = "${importedModuleName}"`);

        if (isInternalImport.test(importedModuleName)) {
          console.info('RECURSE...');
          const importedModuleIntrospectionResults = introspectModule(basePath, normalisedImportedModuleName);

          console.info(`Adding up to ${importedModuleIntrospectionResults.interestingExports.length} interesting locals`);
          interestingLocals.push(
            ...importedModuleIntrospectionResults.interestingExports
              .filter(({ exportName }) => exportName === path.node.name)
              .map((interestingExport) => {
                for (const key in interestingExport.sdkParameters) {
                  if (interestingExport.sdkParameters.hasOwnProperty(key)) {
                    const parameter = interestingExport.sdkParameters[key];
                    if (isSdkParameterInput(parameter)) {
                      const functionCall = path.parent;
                      // TODO Remove more grossness
                      if (isCallExpression(functionCall)) {
                        if (functionCall.arguments.length >= parameter.index) {
                          const argument = functionCall.arguments[parameter.index];
                          if (isStringLiteral(argument)) {
                            parameter.possibleValues.add(argument.value);
                          }
                        }
                      }
                    }
                  }
                }
                return interestingExport;
              })
          );
        }

        if (isAwsSdkImport.test(importedModuleName)) {
          console.info(`***** FOUND AWS SDK IMPORT IN "${exportName}"`);
          console.info(`ancestry = ${JSON.stringify(path.getAncestry().map(({ node: { type } }) => type))}`);

          const callExpressionAncestor = path.getAncestry().find(({ node }) => isCallExpression(node)) as NodePath<CallExpression>;
          if (callExpressionAncestor) {
            console.info(`callExpressionAncestor = ${JSON.stringify(callExpressionAncestor.node)}`);
            // TODO Handle other types
            if (isMemberExpression(callExpressionAncestor.node.callee)) {
              const callee = callExpressionAncestor.node.callee as MemberExpression;
              console.info(`^^^^^ callee = ${JSON.stringify(callee)}`);

              // TODO This is ridiculously hardcoded and needs to handle different types of arguments, references, etc
              const argumentsObjectExpression = callExpressionAncestor.node.arguments[0] as ObjectExpression;
              const sdkParameters = argumentsObjectExpression.properties
                .filter((property) => {
                  const { key } = property as ObjectProperty;
                  // TODO This is hardcoded for DynamoDB (and isn't even complete)
                  return ['TableName', 'IndexName'].includes(key.name);
                })
                .reduce(
                  (result, property) => {
                    const { key, value } = property as ObjectProperty;
                    console.info(JSON.stringify(result, null, 2), key.name, JSON.stringify(value, null, 2));

                    if (isStringLiteral(value)) {
                      return {
                        ...result,
                        [key.name]: {
                          type: 'constant',
                          value: value.value
                        }
                      };
                    }

                    if (isIdentifier(value)) {
                      const identifierBinding = path.scope.getBinding(value.name);
                      if (identifierBinding) {
                        const boundNode = identifierBinding.path.node;
                        console.info(`>>> For binding "${value.name}" we got bound node: ${JSON.stringify(boundNode)}`);
                        if (isVariableDeclarator(boundNode)) {
                          if (isStringLiteral(boundNode.init)) {
                            return {
                              ...result,
                              [key.name]: {
                                type: 'constant',
                                value: boundNode.init.value
                              }
                            }
                          }
                        } else if (isIdentifier(boundNode)) {
                          if (isArrowFunctionExpression(identifierBinding.path.parent)) {
                            const paramIndex = identifierBinding.path.parent.params.findIndex((child) => isIdentifier(child) && child.name === boundNode.name);
                            console.info(`The name of the parameter is "${boundNode.name}" and it is in position ${paramIndex}`);
                            return {
                              ...result,
                              [key.name]: {
                                type: 'input',
                                name: boundNode.name,
                                index: paramIndex,
                                possibleValues: new Set<string>() // TODO Add the default value here
                              }
                            }
                          }
                        }
                      }
                    }

                    // We can't work out the value, so we'll have to have a wide permission
                    // TODO Log levels
                    console.info(`Warning: could not determine value for SDK call parameter "${key.name}"`);
                    return {
                      ...result,
                      [key.name]: { type: 'arbitrary' }
                    };
                  },
                  {});

              const interestingFunction: InterestingFunction = {
                moduleName,
                exportName,
                sdkModuleName: importedModuleName,
                sdkClassName: (binding.path.node as ImportSpecifier).imported.name,
                sdkMethodName: callee.property.name,
                sdkParameters
              };

              console.info(`****** CLASS & METHOD NAME = "${interestingFunction.sdkClassName}.${interestingFunction.sdkMethodName}"`);

              // TODO Handle all functions, but only add to `interestingExports` if it is an export
              interestingExports.push(interestingFunction);
              interestingLocals.push(interestingFunction);
            }
          }
        }

        console.info(`##### ${JSON.stringify(interestingLocals)}`);

        const interestingImports = interestingLocals.filter(
          ({ moduleName: localModuleName, exportName: localExportName }) => {
            console.info(`"${localModuleName}" "${normalisedImportedModuleName}" "${localExportName}" "${path.node.name}"`);
            return localModuleName === normalisedImportedModuleName && localExportName === path.node.name;
          });

        if (interestingImports.length > 0) {
          console.info(`****** Found interesting imports: ${JSON.stringify(interestingImports)}`);
          const interestingFunctions = interestingImports.map((interestingImport) => ({
            ...interestingImport,
            moduleName,
            exportName
          }));

          // TODO Handle all functions, but only add to `interestingExports` if it is an export
          interestingExports.push(...interestingFunctions);
          interestingLocals.push(...interestingFunctions);
        }
      }
    }
  }
};
