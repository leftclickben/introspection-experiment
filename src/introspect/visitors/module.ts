import { Visitor } from '@babel/core';
import { ModuleTraverseState } from '../types';
import { variableDeclaratorVisitor } from './variableDeclarator';
import { Identifier, VariableDeclarator } from '@babel/types';

export const moduleVisitor: Visitor<ModuleTraverseState> = {
  VariableDeclarator: (path, state) => {
    console.info('TRAVERSING VARIABLE');
    path.traverse(variableDeclaratorVisitor, {
      ...state,
      exportName: ((path.node as VariableDeclarator).id as Identifier).name
    });
  }
};
