// TODO Support other code structures
import { Visitor } from '@babel/core';
import { VariableDeclaratorTraverseState } from '../types';
import { arrowFunctionVisitor } from './arrowFunction';

export const variableDeclaratorVisitor: Visitor<VariableDeclaratorTraverseState> = {
  ArrowFunctionExpression: (path, state) => {
    console.info('TRAVERSING ARROW FUNCTION');
    path.traverse(arrowFunctionVisitor, { ...state, interestingLocals: [] });
  }
};
