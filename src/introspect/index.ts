import { introspectModule } from './module';
import { dirname, join, basename } from 'path';
import { writeFileSync } from 'fs';
import { sync as mkdirp } from 'mkdirp';
import { getReportValue } from './report';

// TODO Multiple root modules, but share the parsed information about all modules across the multiple roots
if (require.main === module && process.argv.length > 2) {
  try {
    const [,, ...moduleNames] = process.argv;
    for (const moduleName of moduleNames) {
      const resultsDir = join(process.cwd(), 'results', dirname(moduleName));
      mkdirp(resultsDir);

      const results = introspectModule(join(process.cwd(), 'src'), moduleName);

      const resultsFilename = `${basename(moduleName)}.json`;
      writeFileSync(join(resultsDir, resultsFilename), JSON.stringify(getReportValue(results), null, 2));
    }
  } catch (e) {
    console.error(`Error introspecting modules: ${e}`)
  }
}
