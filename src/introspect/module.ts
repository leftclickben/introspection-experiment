import { parse, traverse } from '@babel/core';
import { existsSync, readFileSync } from 'fs';
import { join } from 'path';
import { InterestingFunction, ModuleIntrospectionResults, ModuleTraverseState } from './types';
import { memo } from './cache';
import { moduleVisitor } from './visitors/module';

export const introspectModule = (basePath: string, moduleName: string): ModuleIntrospectionResults => memo(moduleName, () => {
  console.info(`***** INTROSPECTING MODULE "${moduleName}" *****`);
  console.info(`Base path is "${basePath}"`);

  const filename = join(basePath, `${moduleName}.ts`);

  console.info(`Filename is "${filename}"`);

  if (!existsSync(filename)) {
    throw Error(`File does not exist "${filename}"`);
  }

  const code = readFileSync(filename, 'utf-8');

  const ast = parse(code, { filename, presets: ["@babel/preset-typescript"] });

  if (!ast) {
    throw Error(`Could not resolve AST from file "${filename}"`);
  }

  const interestingExports: InterestingFunction[] = [];
  const interestingLocals: InterestingFunction[] = [];

  traverse<ModuleTraverseState>(ast, moduleVisitor, undefined, { basePath, moduleName, interestingExports, interestingLocals });

  console.info(`***** COMPLETED MODULE "${moduleName}" *****`);
  console.info(JSON.stringify(interestingExports, null, 2));

  return { interestingExports };
});
