export interface ModuleTraverseState {
  basePath: string;
  moduleName: string;
  interestingExports: InterestingFunction[];
  interestingLocals: InterestingFunction[]
}

export interface VariableDeclaratorTraverseState extends ModuleTraverseState {
  exportName: string;
}

export interface SdkParameterConstant {
  type: 'constant';
  value: string | number;
}

export interface SdkParameterInput {
  type: 'input';
  name: string;
  index: number;
  possibleValues: Set<string>;
}

export interface SdkParameterArbitrary {
  type: 'arbitrary';
}

export type SdkParameter = SdkParameterConstant | SdkParameterInput | SdkParameterArbitrary

export const isSdkParameterConstant = (parameter: SdkParameter): parameter is SdkParameterConstant => parameter.type === 'constant';
export const isSdkParameterInput = (parameter: SdkParameter): parameter is SdkParameterInput => parameter.type === 'input';
export const isSdkParameterArbitrary = (parameter: SdkParameter): parameter is SdkParameterArbitrary => parameter.type === 'arbitrary';

export interface InterestingFunction {
  moduleName: string;
  exportName: string;
  sdkModuleName: string;
  sdkClassName: string;
  sdkMethodName: string;
  sdkParameters: Record<string, SdkParameter>; // TODO Should this be a union type with an interface per sdk class/method?
}

export interface ModuleIntrospectionResults {
  interestingExports: InterestingFunction[];
}
