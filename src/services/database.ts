import { DynamoDB } from 'aws-sdk';

export interface DatabaseRecord {
  id?: number;
  name: string;
  description?: string;
}

export const loadRecord = async (table: string, id: string): Promise<DatabaseRecord> => {
  console.info(`Loading data from table ${table}`);
  const { Item: item } = await new DynamoDB.DocumentClient()
    .get({
      TableName: table,
      Key: { id }
    })
    .promise();
  return item as DatabaseRecord;
};

export const saveRecord = async (table: string, record: DatabaseRecord): Promise<void> => {
  console.info(`Saving data ${JSON.stringify(record)} to table ${table}`);
  await new DynamoDB.DocumentClient()
    .put({
      TableName: table,
      Item: record
    })
    .promise();
};
