import { saveRecord } from '../services/database';

interface HandlerEvent {
  body: string;
  pathParameters: {
    id: string;
  }
}

// This is the most basic DynamoDB interaction possible: a direct call from the handler to `get` a single item
export const handler = async ({ pathParameters: { id }, body }: HandlerEvent): Promise<{ statusCode: number, body: string }> => ({
  statusCode: 200,
  body: JSON.stringify(await saveRecord('records', { id, ...JSON.parse(body) }))
});
