import { loadRecord } from '../services/database';

interface HandlerEvent {
  pathParameters: {
    id: string;
  }
}

// This is the most basic DynamoDB interaction possible: a direct call from the handler to `get` a single item
export const handler = async ({ pathParameters: { id } }: HandlerEvent): Promise<{ statusCode: number, body: string }> => ({
  statusCode: 200,
  body: JSON.stringify(await loadRecord('records', id))
});
