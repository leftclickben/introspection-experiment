import { DynamoDB } from 'aws-sdk';

interface HandlerEvent {
  pathParameters: {
    id: string;
  }
}

const TABLE_NAME = 'records';

// This is the most basic DynamoDB interaction possible: a direct call from the handler to `get` a single item
export const handler = async ({ pathParameters: { id } }: HandlerEvent): Promise<{ statusCode: number, body: string }> => {
  const { Item: record } = await new DynamoDB.DocumentClient()
    .get({
      TableName: TABLE_NAME,
      Key: { id }
    })
    .promise();

  return {
    statusCode: 200,
    body: JSON.stringify(record)
  };
};
