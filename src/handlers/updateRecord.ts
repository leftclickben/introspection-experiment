import { loadRecord, saveRecord } from '../services/database';
import { pad } from '../util';

interface HandlerEvent {
  pathParameters: {
    id: string;
  }
}

// This is some terrible code that is nonetheless easy to read and demonstrates AWS SDK usage (DynamoDB)
export const handler = async ({ pathParameters: { id } }: HandlerEvent): Promise<void> => {
  // This function calls the AWS SDK, so we are interested in this, our handler needs a permission (this is a function call expression)
  const record = await loadRecord('records', id);
  // This just demonstrates a call to a function we don't care about (but we still need to know that we don't care)
  record.description += pad('(updated)', 32);
  // This also calls the AWS SDK, so we need a permission for this (this is a function call statement)
  await saveRecord('records', record);
};
