export const pad = (s: string, length: number): string => {
  const a = new Array(length);
  s.split('').forEach((c, i) => { a[i] = c; });
  return a.join('');
};
