# Introspection experiment

This repository contains an experiment on introspecting some JavaScript code, to determine which of the specified entry points (in this case, Lambda handlers) uses which interesting functions from external packages (in this case, the AWS SDK).

**DISCLAIMER** This code is terrible!  It is the outcome of my first investigations into Babel and how to use it to introspect some other code (which is also, intentionally, terrible).  It's not elegant, it's very specific, it contains up to 8 nested `if` statements, and about a billion `TODO` comments, and you really should not even look at it.  Look at this code at your own risk of damage to your eyes or your brain.

**CAVEAT** This introspection code handles only a small portion of the possible ways the code could be written, so it will not work in a generic sense.  The purpose was to see what was possible, before trying to make something generic and all-encapsulating.

> TODO Blog post link

## Usage

To run the introspection on all 4 of the given entry points, redirecting log messages to a datestamped file:

```
npm run introspect ./handlers/updateRecord ./handlers/getRecord ./handlers/putRecord ./handlers/getRecordDirect > logs/$(date +%Y%m%d%H%M%S).log
```

## Inputs

There are 4 `handler` functions that represent Lambda entry points.  They are all intended to look like very simple API handlers.  Two of these are equivalent, both retrieve an item from the database by its `id`, with one directly calling the AWS SDK and the other using another internal module (`services/database`).  The other two represent a "put" and an "update" to prove the point that different sets of types of permissions can be obtained.

**ATTENTION** The `updateRecord` handler loads the data, modifies it, then saves it back, which definitely not the right way to use DynamoDB.  Don't do it!  But I wanted to have the two different calls in a single handler just to prove that it would find both.

**NOTE** The parameters to these functions are not correct for a Lambda proxy integration (but suppose it could be the result of the VTL transforms in the Lambda integration).

## Outputs

The results are written as JSON files in the `results` directory.  Each file contains a record of the handler, the AWS SDK functions that it calls, and the possible values to the parameters of those functions.

**RESTRICTION** This only works for DynamoDB and only in certain circumstances!

This is an example of the output for the `updateRecord` handler:

```json
{
  "interestingExports": [
    {
      "moduleName": "./handlers/updateRecord",
      "exportName": "handler",
      "sdkModuleName": "aws-sdk",
      "sdkClassName": "DynamoDB",
      "sdkMethodName": "get",
      "sdkParameters": {
        "TableName": [
          "records"
        ]
      }
    },
    {
      "moduleName": "./handlers/updateRecord",
      "exportName": "handler",
      "sdkModuleName": "aws-sdk",
      "sdkClassName": "DynamoDB",
      "sdkMethodName": "put",
      "sdkParameters": {
        "TableName": [
          "records"
        ]
      }
    }
  ]
}
```

And here is the output for `getRecord`:

```json
{
  "interestingExports": [
    {
      "moduleName": "./handlers/getRecord",
      "exportName": "handler",
      "sdkModuleName": "aws-sdk",
      "sdkClassName": "DynamoDB",
      "sdkMethodName": "get",
      "sdkParameters": {
        "TableName": [
          "records"
        ]
      }
    }
  ]
}
```
